extern crate clap;
use clap::{App, Arg};

#[macro_use]
extern crate slog;
extern crate slog_scope;
extern crate slog_stdlog;
extern crate slog_term;
#[macro_use]
extern crate log;

use colored::*;
use slog::Drain;
use std::fs::OpenOptions;

mod find_os;
mod nt28;


fn main() {
    let matches = App::new("pyranha")
        .version("0.1")
        .author("r3dlight")
        .about("Hardening for Debian based GNU/Linux distribution")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("log")
                .short("l")
                .long("log")
                .value_name("LOG")
                .help("Sets a custom Log file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbosity")
                .multiple(true)
                .help("Sets to level of verbosity"),
        )
        .get_matches();

    let config = matches.value_of("config").unwrap_or("default.conf");
    let log_path = matches.value_of("log").unwrap_or("pyranha.log");

    println!("Config : {}", config.green());
    println!("Logfile : {}", log_path.green());
    //println!("{} {} !", "it".green(), "works".blue().bold());

    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(log_path)
        .unwrap();

    // create logger
    let decorator = slog_term::PlainSyncDecorator::new(file);
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let logger = slog::Logger::root(drain, o!());

    // slog_stdlog uses the logger from slog_scope, so set a logger there
    let _guard = slog_scope::set_global_logger(logger);

    // register slog_stdlog as the log handler with the log crate
    slog_stdlog::init().unwrap();

    let contents = find_os::find_os();
    println!("Distribution découverte : {}", contents.green());
    info!("Distribution découverte : {}\n", contents);
    //Should check the level
    nt28::r1();
    nt28::r2();
    nt28::r3();
    nt28::r4();
    nt28::r5();
    nt28::r6();
    nt28::r7();
    nt28::r8();
    nt28::r9();
    nt28::r10();
    nt28::r11();

}
