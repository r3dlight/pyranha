
use colored::*;
use netstat::*;
use regex::Regex;
use std::fs;
use std::path::Path;
use std::process;

pub fn r1() {
    println!(
        "{} {}",
        "ANSSI/NT-28/R1:".cyan(),
        "Minimisation des services installés".underline()
    );
    println!("{}","\t\"Seuls les composants strictement nécessaires au service rendu par le système doivent être installés.
\tTout service (particulièrement en écoute active sur le réseau) est un élément sensible.
\tSeuls ceux connus et requis pour le fonctionnement et la maintenance doivent être résidents. 
\tCeux dont la présence ne peut être justifiée doivent être désactivés, désinstallés ou supprimés.
\tEX: Redirection de port SSH, Serveur WWW : Indexation des répertoires\"".yellow().italic());

    println!(
        "{} {}",
        "ANSSI/NT-28/R1:".on_cyan(),
        "Merci de désinstaller tous les services et paquets inutiles !".bright_yellow()
    );
    info!(
        "{} {}",
        "ANSSI/NT-28/R1:".on_cyan(),
        "Merci de désinstaller tous les services et paquets inutiles !".bright_yellow()
    );
}


pub fn r2() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R2:".cyan(),
        "Minimisation de la configuration".underline()
    );
    println!("{}","\t\"Les fonctionnalités configurées au niveau des services démarrés doivent être limitées au strict 
    \tnécessaire.\"".yellow().italic());
    println!(
        "{} {}",
        "ANSSI/NT-28/R2:".on_cyan(),
        "Merci de désactiver les fontionnalités inutiles pour vos services.".bright_yellow()
    );
    info!(
        "{} {}",
        "ANSSI/NT-28/R2:".on_cyan(),
        "Merci de désactiver les fontionnalités inutiles pour vos services.".bright_yellow()
    );
}

pub fn r3() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R3:".cyan(),
        "Principe du moindre privilège".underline()
    );
    println!("{}","\t\"Les services et exécutables disponibles sur le système doivent faire l’objet d’une analyse
\tafin de connaître les privilèges qui leurs sont associés, et doivent ensuite être configurés
\tet intégrés en vue d’en utiliser le strict nécessaire.\"".yellow().italic());
    println!(
        "{} {}",
        "ANSSI/NT-28/R3:".on_cyan(),
        "Merci d'accorder les droits strictement nécessaires aux utilisateurs / groupes."
            .bright_yellow()
    );
    info!(
        "{} {}",
        "ANSSI/NT-28/R3:".on_cyan(),
        "Merci d'accorder les droits strictement nécessaires aux utilisateurs / groupes."
            .bright_yellow()
    );
}

pub fn r4() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R4:".cyan(),
        "Utilisation des fonctionnalités de contrôle d’accès".underline()
    );
    println!("{}","\t\"Il est recommandé d’utiliser les fonctionnalités de contrôle d’accès obligatoire (MAC) en
\tplus du traditionnel modèle utilisateur Unix (DAC), voire éventuellement de les combiner
\tavec des mécanismes de cloisonnement.
\tEX: Utilisation de nsjail, vserver, espaces de nommage (Namespaces), LXC, 
\tSecure computing (seccomp), capacités POSIX ou seccomp via systemd...\"".yellow().italic());
    println!(
        "{} {}",
        "ANSSI/NT-28/R4:".on_cyan(),
        "Merci d'utiliser du control d'accès obligatoire (MAC).".bright_yellow()
    );
    info!(
        "{} {}",
        "ANSSI/NT-28/R4:".on_cyan(),
        "Merci d'utiliser du control d'accès obligatoire (MAC).".bright_yellow()
    );
}

pub fn r5() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R5:".cyan(),
        "Principe de défense en profondeur".underline()
    );
    println!(
        "{}",
        "\t\"Sous Unix et dérivés, la défense en profondeur doit reposer sur une combinaison de
\tbarrières qu’il faut garder indépendantes les unes des autres. Par exemple :
\t\t– authentification nécessaire avant d’effectuer des opérations, notamment quand elles
\t\tsont privilégiées ;
\t\t– journalisation centralisée d’évènements au niveau systèmes et services ;
\t\t– priorité à l’usage de services qui implémentent des mécanismes de cloisonnement et/ou
\t\tde séparation de privilèges ;
\t\t– utilisation de mécanismes de prévention d’exploitation\""
            .yellow()
            .italic()
    );
    println!(
        "{} {}",
        "ANSSI/NT-28/R5:".on_cyan(),
        "Merci de mettre en place une défense en profondeur.
\t\tEX : Firewalld, iptables, fail2ban, authentification via kerberos..."
            .bright_yellow()
    );
}

pub fn r6() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R6:".cyan(),
        "Cloisonnement des services réseau".underline()
    );
    println!(
        "{}",
        "\t\"Les services réseau doivent autant que possible être hébergés sur des environnements
\tdistincts. Cela évite d’avoir d’autres services potentiellement affectés si l’un d’eux se
\tretrouve compromis sous le même environnement.\""
            .yellow()
            .italic()
    );
    let af_flags = AddressFamilyFlags::IPV4 | AddressFamilyFlags::IPV6;
    let proto_flags = ProtocolFlags::TCP | ProtocolFlags::UDP;
    let sockets_info = get_sockets_info(af_flags, proto_flags).unwrap();
    let mut tcp_count = 0;
    let mut udp_count = 0;
    for si in sockets_info {
        match si.protocol_socket_info {
            ProtocolSocketInfo::Tcp(_tcp_si) =>
            //{
            //println!(
            //"TCP {}:{} -> {}:{} {:?} - {}",
            //tcp_si.local_addr,
            //tcp_si.local_port,
            //tcp_si.remote_addr,
            //tcp_si.remote_port,
            //si.associated_pids,
            //tcp_si.state
            //);
            {
                tcp_count = tcp_count + 1
            }
            //}
            ProtocolSocketInfo::Udp(_udp_si) =>
            //{
            //println!(
            //"UDP {}:{} -> *:* {:?}",
            //udp_si.local_addr, udp_si.local_port, si.associated_pids
            //);
            {
                udp_count = udp_count + 1
            } //}
        }
    }
    println!(
        "{} {} {}",
        "\n\t> Actuellement",
        tcp_count.to_string().bright_green(),
        "connexions TCP découvertes !"
    );
    println!(
        "{} {} {}",
        "\t> Actuellement",
        udp_count.to_string().bright_green(),
        "connexions UDP découvertes !"
    );
}

pub fn r7() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R7:".cyan(),
        "Journalisation de l’activité des services".underline()
    );
    println!("{}","\t\"Les activités du système et des services en cours d’exécution doivent être journalisées et
\tarchivées sur un système externe, non local.\"".yellow().italic());
    //println!("{} {}", "Service syslog installé sur le système : ".yellow(), syslog)
}

pub fn r8() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R8:".cyan(),
        "Mises à jour régulières".underline()
    );
    println!("{}","\t\"Il est recommandé d’avoir une procédure de mise à jour de sécurité régulière et réactive.
\tL’activité de veille peut se faire par l’inscription à des listes de diffusion (équipes sécurité
\tdes composants et applications installés et de leurs éditeurs, flux RSS de CERT, etc.).\"".yellow().italic());

    println!(
        "{} {}",
        "ANSSI/NT-28/R8:".on_cyan(),
        "Merci de mettre à jour votre système !".bright_yellow()
    );
    info!(
        "{} {}",
        "ANSSI/NT-28/R8:".on_cyan(),
        "Merci de mettre à jour votre système !".bright_yellow()
    );
}

pub fn r9() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R9:".cyan(),
        "Configuration matérielle".underline()
    );
    println!("{}","\t\"Il est conseillé d’appliquer les recommandations de configuration mentionnées dans
\tla note technique Recommandations de configuration matérielle de postes clients et
\tserveurs x86.\"".yellow().italic());

}

pub fn r10() {
    println!(
        "{} {}",
        "\nANSSI/NT-28/R10:".cyan(),
        "Architecture 32 et 64 bits".underline()
    );
    println!("{}","\t\"Lorsque la machine le supporte, préférer l’installation d’une distribution GNU/Linux en
\tversion 64 bits plutôt qu’en version 32 bits.".bright_yellow());
    //println!("{}", std::mem::size_of::<usize>()*8);
    let architecture = std::mem::size_of::<usize>() * 8;
    let arch_regex = Regex::new(r"64").unwrap();

    let matched = arch_regex.is_match(&architecture.to_string());
    //println!("{:?}", matched);
    match matched {
        true => {
            println!(
                "{} {}",
                "ANSSI/NT-28/R10:".on_cyan(),
                "Architecture 64 bits détectée : conforme !".bright_green()
            );
            info!(
                "{} {}",
                "ANSSI/NT-28/R10:".on_cyan(),
                "Architecture 64 bits détectée : conforme !".bright_green()
            )
        }

        false => {
            println!(
                "{} {}",
                "ANSSI/NT-28/R10:".on_cyan(),
                "Architecture détectée < 64 bits !".bright_red().bold()
            );
            info!(
                "{} {}",
                "ANSSI/NT-28/R10:".on_cyan(),
                "Architecture détectée < 64 bits !".bright_red().bold()
            )
        }
    }
}

pub fn r11() {
    let grub_path;
    println!(
        "{} {}",
        "\nANSSI/NT-28/R11:".cyan(),
        "Directive de configuration de l’IOMMU".underline()
    );
    println!(
        "{} {}",
        "ANSSI/NT-28/R11:".cyan(),
        "Checking grub configuration..."
    );
    info!("ANSSI/NT-28/R11: Checking grub configuration...");
    if Path::new("/etc/default/grub").exists() {
        println!(
            "{} {}",
            "ANSSI/NT-28/R11:".cyan(),
            "Found file : /etc/default/grub !"
        );
        info!("ANSSI/NT-28/R11: Found file : /etc/default/grub !");
        grub_path = "/etc/default/grub";
    } else if Path::new("/boot/grub/menu.lst").exists() {
        grub_path = "/boot/grub/menu.lst";
        println!("ANSSI/NT-28/R11: Found file : /etc/redhat-release !");
        info!("ANSSI/NT-28/R11: Found file : /etc/redhat-release !");
    } else {
        error!("Cannot find grub configuration file !");
        process::exit(0);
    }

    let contents = fs::read_to_string(grub_path).expect("Something went wrong reading the file");
    //should make a better regex to match spaces and tabs
    let grub_regex = Regex::new(r"iommu=force").unwrap();

    let matched = grub_regex.is_match(&contents);
    //println!("{:?}", matched);
    match matched {
        true => println!(
            "{} {}",
            "ANSSI/NT-28/R11:".on_cyan(),
            "Configuration IOMMU : conforme !".bright_green()
        ),
        false => println!(
            "{} {}",
            "ANSSI/NT-28/R11:".on_cyan(),
            "Configuration IOMMU : non conforme !".bright_red().bold()
        ),
    }
}
