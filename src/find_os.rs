use colored::*;
use std::fs;
use std::path::Path;
use std::process;

pub fn find_os() -> String {
    //println!("{}", Path::new("/etc/hosts").exists());
    let distro;

    if Path::new("/etc/debian_version").exists() {
        println!(
            "{} {}",
            "Fichier trouvé :".bright_white(),
            "/etc/debian_version !".green()
        );
        info!("Fichier trouvé : /etc/debian_version !");
        distro = "/etc/debian_version";
    } else if Path::new("/etc/redhat-release").exists() {
        println!("{}", "Fichier trouvé : /etc/redhat-release !".green());
        info!("Fichier trouvé : /etc/redhat-release !");
        distro = "/etc/redhat-release";
    } else {
        error!("Version GNU/Linux non supportée");
        process::exit(0);
    }
    //let distro="/etc/debian_version";
    info!("Reading file {}", distro.yellow());

    let contents = fs::read_to_string(distro).expect("Something went wrong reading the file");

    return contents;
}
